import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LayoutComponent } from './layout.component';
import { MaterialTableComponent} from '../material-table/material-table.component';
import { HttpRequestComponent } from '../http-request/http-request.component';

const routes: Routes = [
  {
    path: '', 
    component: LayoutComponent,
    children: [{
      path: 'material-table',
      component: MaterialTableComponent
    },{
      path: 'http-request',
      component: HttpRequestComponent
    }]
  },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
export const routedComponents = [LayoutComponent];