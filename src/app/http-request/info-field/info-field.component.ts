import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-info-field',
  templateUrl: './info-field.component.html',
  styleUrls: ['./info-field.component.scss']
})
export class InfoFieldComponent implements OnInit {
  infos:{
    name: string,
    age: Number,
    gender: string
  }[] = [{
    name: 'Hoang Vu Dat',
    age: 22,
    gender: 'male'
  },{
    name: 'Hoang Vu Dat',
    age: 22,
    gender: 'male'
  }];

  constructor() { }

  ngOnInit() {
  }

}
