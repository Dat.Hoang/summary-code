import { Pipe, PipeTransform } from '@angular/core';
import { isNgTemplate } from '@angular/compiler';
import { iterateListLike } from '@angular/core/src/change_detection/change_detection_util';

@Pipe({
  name: 'searchSidebar'
})
export class SearchSidebarPipe implements PipeTransform {

  transform(value: any, filter: string): any {
    let tempArray = [];
    for(const item in value){
      if(value[item].name 
        && filter 
        && value[item].name.toLowerCase().includes(filter.toLowerCase())){
          tempArray.push(value[item]);
      }
    }

    if(filter == '' && tempArray.length === 0) return value;

    return tempArray;
  }

}
