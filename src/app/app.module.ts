import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { LayoutModule } from './layout/layout.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MaterialTableComponent } from './material-table/material-table.component';
import { HttpRequestComponent } from './http-request/http-request.component';
import { InputFieldComponent } from './http-request/input-field/input-field.component';
import { InfoFieldComponent } from './http-request/info-field/info-field.component';

@NgModule({
  declarations: [
    AppComponent,
    MaterialTableComponent,
    HttpRequestComponent,
    InputFieldComponent,
    InfoFieldComponent,
  ],
  imports: [
    BrowserModule,
    LayoutModule,
    AppRoutingModule,
    NgxDatatableModule,
    NgSelectModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
