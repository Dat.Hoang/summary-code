import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';

import { LayoutComponent } from './layout.component';
import { LayoutRoutingModule } from './layout.routing';
import { LayoutContentComponent } from './layout-content/layout-content.component';
import { SearchSidebarPipe } from '../pipes/search-sidebar.pipe';


@NgModule({
  declarations: [
    LayoutComponent,
    LayoutContentComponent,
    SearchSidebarPipe
  ],
  imports: [
    NgxDatatableModule,
    NgbModule,
    LayoutRoutingModule,
    CommonModule,
    FormsModule,
    NgSelectModule
  ],
  bootstrap: [LayoutComponent]
})
export class LayoutModule { }
