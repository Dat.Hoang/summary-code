import { Component, OnInit, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-input-field',
  templateUrl: './input-field.component.html',
  styleUrls: ['./input-field.component.scss']
})
export class InputFieldComponent implements OnInit {
  afterSubmit = new EventEmitter<{name:string, gender: string, age: Number}>();

  title:string = "Input information";
  gender:string='male';
  name:string;
  age:string;

  infoObject;

  constructor() { }

  ngOnInit() {
  }

  getAllInformation(){
    this.infoObject = {
      name:this.name,
      gender: this.gender,
      age: +this.age
    }

    this.afterSubmit.emit(this.infoObject);

  }
}
