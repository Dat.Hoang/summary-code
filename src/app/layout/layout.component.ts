import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
  listChooser: any[];
  searchContent: string = '';

  constructor() { }

  ngOnInit() {
    this.createListChooser();
  }

  createListChooser(){
    this.listChooser = [
      {
        name:'Material table',
        url: '/material-table'
      },{
        name:'Http request',
        url: '/http-request'
      }];
  }

}
